#pragma once

#include "Actors/actor.h"

class Player : public Ming3D::Actor
{
    DEFINE_CLASS(Player, Ming3D::Actor)

private:
    static void InitialiseClass();


public:
    void InitialisePlayer();
};
