
#include <iostream>

#include "GameEngine/game_engine.h"
#include "World/world.h"
#include "Actors/actor.h"
#include "Model/model_helper.h"
#include "Components/camera_component.h"
#include "Model/primitive_factory.h"
#include "land_area.h"
#include "Input/input_manager.h"
#include <algorithm>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include "player_controller.h"

using namespace Ming3D;

int main()
{
    GameEngine* gameEngine = new GameEngine();
	gameEngine->Initialise();

    gameEngine->SetMainWindowSize(1366, 768);

    LandArea* mLandArea = new LandArea(36, 36);

    PlayerController* playerController = new PlayerController();
    gameEngine->GetWorld()->AddActor(playerController);

    while (true)
    {
        gameEngine->Update();
    }

	return 0;
}
