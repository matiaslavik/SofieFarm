#pragma once

#include "tile_occupancy.h"
#include <vector>

/*
* An object that is placed on one or more land tiles.
* This can be a plant, a building, decoration, etc.
**/
class LandUnit
{
private:
    /* Tiles occupied by this unit. */
    std::vector<TileOccupancy> mOccupeidTiles;
    /* Tiles that need to be occupied by this unit, to place it. Relative to centre. */
    std::vector<TileOccupancy> mRequiredTiles;
};
