#pragma once

#include "glm/vec2.hpp"
#include "tile_slot.h"

struct TileOccupancy
{
public:
    glm::ivec2 mTile;
    ETileSlot mTileSLot;
};
