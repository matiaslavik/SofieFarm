#include "player_controller.h"
#include "GameEngine/game_engine.h"
#include "Model/model_helper.h"
#include "World/world.h"
#include "Components/camera_component.h"
#include "Input/input_manager.h"
#include "glm/vec3.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include <algorithm>
#include "land_area.h"
#include <cmath>

IMPLEMENT_CLASS(PlayerController)

void PlayerController::InitialiseClass()
{

}

PlayerController::PlayerController()
{
    // Create player
    mPlayer = new Player();
    
    // Create mesh container
    mMeshContainer = new Actor();
    Ming3D::ModelLoader::LoadModel(std::string("Resources/FarmAnimalsQuaternius/FBX/Cow.fbx"), mMeshContainer);
    mMeshContainer->GetTransform().SetLocalPosition(glm::vec3(0.0f, 0.0f, 0.0f));
    mMeshContainer->GetTransform().SetLocalScale(glm::vec3(0.001f, 0.001f, 0.001f));
    mMeshContainer->GetTransform().SetParent(&mPlayer->GetTransform());

    Ming3D::GGameEngine->GetWorld()->AddActor(mPlayer);

    Actor* test = new Actor();
    Ming3D::ModelLoader::LoadModel(std::string("Resources/Custom/Lettuce.dae"), test);
    test->GetTransform().SetLocalPosition(glm::vec3(3.5f, 0.0f, 3.5f));
    Ming3D::GGameEngine->GetWorld()->AddActor(test);

    // Create camera actor
    mCameraActor = new Ming3D::Actor();
    mCameraActor->AddComponent<Ming3D::CameraComponent>();
    Ming3D::GGameEngine->GetWorld()->AddActor(mCameraActor);
}

void PlayerController::Tick(float deltaTime)
{
    Ming3D::Actor::Tick(deltaTime);

    HandlePlayerMovementInput(deltaTime);
    HandlePlayerActionInput();
}

void PlayerController::HandlePlayerMovementInput(float deltaTime)
{
    // Player movement
    glm::vec2 axisLeft = Ming3D::GGameEngine->GetInputManager()->GetAxis2D(Ming3D::EAxis2D::ControllerAxisLeft);
    if(axisLeft.x != 0.0f || axisLeft.y != 0.0f)
    {
        // Calculate right/forward vector for movement
        glm::vec3 right = mCameraActor->GetTransform().GetRight();
        glm::vec3 forward = glm::cross(right, glm::vec3(0.0f, 1.0f, 0.0f));

        // Update player position
        glm::vec3 playerPos = mPlayer->GetTransform().GetWorldPosition();
        glm::vec3 playerPosNew = playerPos + (right * axisLeft.x + forward * axisLeft.y) * mPlayerMovementSpeed * deltaTime;
        mPlayer->GetTransform().SetWorldPosition(playerPosNew);

        // Rotate player towards direction of movement
        glm::mat4 lookatMat = glm::lookAt(playerPosNew, playerPos, glm::vec3(0, 1, 0));
        mPlayer->GetTransform().SetWorldRotation(glm::inverse(lookatMat));

    }

    // Calculate camera pitch/yaw/roll
    glm::vec2 axisRight = Ming3D::GGameEngine->GetInputManager()->GetAxis2D(Ming3D::EAxis2D::ControllerAxisRight);
    mCamYaw = mCamYaw + axisRight.x * deltaTime * mCamRotSpeed;
    mCamPitch = std::clamp(mCamPitch + axisRight.y * deltaTime * mCamRotSpeed, -1.0f, 0.0f);

    // Set camera position/rotation
    glm::quat camRot = glm::angleAxis(mCamYaw, glm::vec3(0,1,0)) * glm::angleAxis(mCamPitch, glm::vec3(1,0,0)) ;
    glm::mat4 camAttachMat = glm::translate(glm::mat4(1.0f), mPlayer->GetTransform().GetWorldPosition() + glm::vec3(0.0f, 1.0f, 0.0f)) * glm::toMat4(camRot);
    mCameraActor->GetTransform().SetWorldPosition(camAttachMat * glm::vec4(0.0f, 0.0f, 5.0f, 1.0f));
    mCameraActor->GetTransform().SetWorldRotation(camRot);
}

void PlayerController::HandlePlayerActionInput()
{
    glm::vec3 playerPos = mPlayer->GetTransform().GetWorldPosition();

    if(Ming3D::GGameEngine->GetInputManager()->GetKeyDown(Ming3D::KeyCode::Pad_A))
    {
        LandArea::instance->SetTileSurface(static_cast<int>(playerPos.x), static_cast<int>(playerPos.z), ELandSurface::PlowedLand);
    }
}
