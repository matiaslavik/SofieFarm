#include "land_area.h"
#include <algorithm>
#include "glm/vec3.hpp"
#include "glm/vec2.hpp"
#include <cstring>
#include <string>
#include <cassert>

#include "GameEngine/game_engine.h"
#include "Actors/actor.h"
#include "World/world.h"
#include "Model/material_factory.h"
#include "Texture/texture_loader.h"

LandArea* LandArea::instance = nullptr;

LandArea::LandArea(const int width, const int height)
{
    assert(instance == nullptr); // TODO
    instance = this;

    mDimX = width;
    mDimY = height;
    mLandTiles.reserve(width * height);

    mSectionDimX = static_cast<int>(width / SECTION_SIZE);
    mSectionDimY = static_cast<int>(height / SECTION_SIZE);

    CreateLandMeshes();

    SetTileSurface(2, 2, ELandSurface::PlowedLand); // TEST
}

void LandArea::CreateLandMeshes()
{
    for(int ySect = 0; ySect < mSectionDimY; ySect++)
    {
        for(int xSect = 0; xSect < mSectionDimX; xSect++)
        {
            const int startTileX = xSect * SECTION_SIZE;
            const int startTileY = ySect * SECTION_SIZE;
            const int width = std::min(SECTION_SIZE, mDimX - startTileX);
            const int height = std::min(SECTION_SIZE, mDimY - startTileY);

            std::vector<LandVertex> vertices;
            vertices.reserve(width * height);
            std::vector<unsigned int> indices;
            indices.reserve(static_cast<size_t>((width * height) * 1.5f));

            for(int iy = startTileY; iy < startTileY + height; iy++)
            {
                for(int ix = startTileX; ix < startTileX + width; ix++)
                {
                    int vi = vertices.size();

                    const float uvOffset = 0.001f;

                    LandVertex vt1, vt2, vt3, vt4;
                    vt1.mPosition = glm::vec3(ix * TILE_SIZE, 0.0f, iy * TILE_SIZE);
                    vt1.mNormal = glm::vec3(0.0f, 1.0f, 0.0f);
                    vt2.mPosition = glm::vec3((ix + 1) * TILE_SIZE, 0.0f, iy * TILE_SIZE);
                    vt2.mNormal = glm::vec3(0.0f, 1.0f, 0.0f);
                    vt3.mPosition = glm::vec3(ix * TILE_SIZE, 0.0f, (iy + 1) * TILE_SIZE);
                    vt3.mNormal = glm::vec3(0.0f, 1.0f, 0.0f);
                    vt4.mPosition = glm::vec3((ix + 1) * TILE_SIZE, 0.0f, (iy + 1) * TILE_SIZE);
                    vt4.mNormal = glm::vec3(0.0f, 1.0f, 0.0f);

                    SetTileSurfaceUV(vt1, vt2, vt3, vt4, ELandSurface::Grass);

                    vertices.push_back(vt1);
                    vertices.push_back(vt2);
                    vertices.push_back(vt3);
                    vertices.push_back(vt4);

                    indices.push_back(vi);
                    indices.push_back(vi + 2);
                    indices.push_back(vi + 1);
                    indices.push_back(vi + 1);
                    indices.push_back(vi + 2);
                    indices.push_back(vi + 3);

                    LandTile* tile = new LandTile();
                    tile->mLandSurface = ELandSurface::Grass;
                    mLandTiles.push_back(tile);
                }
            }

            Ming3D::Mesh* mesh = new Ming3D::Mesh();
            mesh->mVertexData = new Ming3D::Rendering::VertexData({ Ming3D::Rendering::EVertexComponent::Position, Ming3D::Rendering::EVertexComponent::Normal, Ming3D::Rendering::EVertexComponent::TexCoord }, indices.size());
            mesh->mIndexData = new Ming3D::Rendering::IndexData(indices.size());

            memcpy(mesh->mVertexData->GetDataPtr(), vertices.data(), vertices.size() * sizeof(LandVertex));
            memcpy(mesh->mIndexData->GetData(), indices.data(), indices.size() * sizeof(unsigned int));

            // TEST
            Ming3D::Actor* actor = new Ming3D::Actor();
            Ming3D::MeshComponent* meshComponent = actor->AddComponent<Ming3D::MeshComponent>();
            meshComponent->SetMesh(mesh, true);
            Ming3D::Material* material = Ming3D::MaterialFactory::CreateMaterial(Ming3D::GGameEngine->GetResourceDirectory() + std::string("/Shaders/defaultshader.cgp"));
            Ming3D::Texture* texture = Ming3D::TextureLoader::LoadTextureData(std::string("Resources/LandTileMap.png"));
            texture->mTextureInfo.mTextureWrapMode = Ming3D::TextureWrapMode::Clamp; // TODO: When creating texture?
            material->SetTexture(0, texture); // TODO: create override with only one parameter
            material->SetShaderUniformVec2("_textureTiling", glm::vec2(1.0f, 1.0f));
            meshComponent->SetMaterial(material);
            Ming3D::GGameEngine->GetWorld()->AddActor(actor);

            mSections.push_back(meshComponent);
        }
    }
}

void LandArea::SetTileSurfaceUV(LandVertex& vt1, LandVertex& vt2, LandVertex& vt3, LandVertex& vt4, ELandSurface surface)
{
    const float tileWidth = 1.0f / 3.0f;

    glm::vec2 uv;
    switch(surface)
    {
        case ELandSurface::Grass:
        {
            uv = glm::vec2(tileWidth * 1.0f, tileWidth * 2.0f);
            break;
        }
        case ELandSurface::PlowedLand:
        {
            uv = glm::vec2(tileWidth * 0.0f, tileWidth * 1.0f);
            break;
        }
        // TODO: other surfaces
    }

    const float uvOffset = 0.001f;

    vt1.mTexCoord = glm::vec2(uv.x + uvOffset, uv.y + uvOffset);
    vt2.mTexCoord = glm::vec2(uv.x + tileWidth - uvOffset, uv.y + uvOffset);
    vt3.mTexCoord = glm::vec2(uv.x + uvOffset, uv.y + tileWidth - uvOffset);
    vt4.mTexCoord = glm::vec2(uv.x + tileWidth - uvOffset, uv.y + tileWidth - uvOffset);
}

void LandArea::SetTileSurface(int x, int y, ELandSurface surface)
{
    const int sectionIndex = (y / SECTION_SIZE) * mSectionDimX + (x / SECTION_SIZE);
    assert(sectionIndex < mSections.size());
    Ming3D::MeshComponent* meshComp = mSections[sectionIndex];
    Ming3D::Mesh* mesh = meshComp->GetMesh();

    const int idx = x % SECTION_SIZE;
    const int idy = y % SECTION_SIZE;
    const int vertID = (idy * SECTION_SIZE + idx) * 4;

    LandVertex* vertices = mesh->mVertexData->GetDataAs<LandVertex>();
    LandVertex& vt1 = vertices[vertID];
    LandVertex& vt2 = vertices[vertID + 1];
    LandVertex& vt3 = vertices[vertID + 2];
    LandVertex& vt4 = vertices[vertID + 3];
    SetTileSurfaceUV(vt1, vt2, vt3, vt4, surface);

    meshComp->ReuploadVertexData();

    LandTile* tile = mLandTiles[y * mDimX + x];
    tile->mLandSurface = surface;
}
