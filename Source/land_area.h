#pragma once

#include <vector>
#include "land_tile.h"
#include "Model/mesh.h"
#include "Components/mesh_component.h"

struct LandVertex
{
public:
    glm::vec3 mPosition;
    glm::vec3 mNormal;
    glm::vec2 mTexCoord;
};

class LandArea
{
private:
    const int SECTION_SIZE = 9;
    const float TILE_SIZE = 1.0f;

    int mDimX;
    int mDimY;
    std::vector<LandTile*> mLandTiles;

    int mSectionDimX;
    int mSectionDimY;

    std::vector<Ming3D::MeshComponent*> mSections;

    void CreateLandMeshes();
    void SetTileSurfaceUV(LandVertex& vt1, LandVertex& vt2, LandVertex& vt3, LandVertex& vt4, ELandSurface surface);

public:
    LandArea(const int width, const int height);

    static LandArea* instance;

    void SetTileSurface(int x, int y, ELandSurface surface);
};
