#pragma once

enum class ELandSurface
{
    Grass,
    PlowedLand,
    Water
};
