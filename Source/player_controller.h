#pragma once

#include "player.h"

class PlayerController : public Ming3D::Actor
{
    DEFINE_CLASS(PlayerController, Ming3D::Actor)

private:
    static void InitialiseClass();

private:
    Player* mPlayer;
    Ming3D::Actor* mMeshContainer;
    Ming3D::Actor* mCameraActor;

    const float mPlayerMovementSpeed = 2.0f;
    const float mCamRotSpeed = 2.0f;
    float mCamPitch = 0.0f;
    float mCamYaw = 0.0f;

    void HandlePlayerMovementInput(float deltaTime);
    void HandlePlayerActionInput();

public:
    PlayerController();
    virtual void Tick(float deltaTime) override;
};
