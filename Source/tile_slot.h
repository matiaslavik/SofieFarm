#pragma once

enum class ETileSlot
{
    Centre,
    Left,
    Right,
    Top,
    Bottom
};
